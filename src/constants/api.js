const host_dic = {
  dev: "https://easymock.thyiad.top/mock/5fabb662f398e40020f988dd/boilerplate", // 前期dev使用mock，后期改为test
  test: "https://dm-medical-test.zhonganonline.com",
  pre: "https://dm-medical-uat.zhonganonline.com",
  prd: "https://dm-medical.zhonganonline.com"
};

let _host = host_dic[process.env.VUE_APP_ENV];
console.log("api.js: " + process.env.VUE_APP_ENV);
export const HOST = _host || "";

export const LOGIN = HOST + "/open/login";
export const GET_USER_INFO = HOST + "/getUserInfo";
export const UPLOAD = HOST + "/user/upload";

export const TABLE_LIST = HOST + "/tableList";

export const REGISTER = HOST + "/commonOK";
export const SEND_FORGET_VERIFY_CODE = HOST + "/commonOK";
export const FORGET_PASSWORD = HOST + "/commonOK";
export const UPDATE_PROFILE = HOST + "/commonOK";
export const UPDATE_PASSWORD = HOST + "/commonOK";
