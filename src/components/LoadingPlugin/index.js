import Loading from './Loading';

const LoadingPlugin = {
  install(Vue){
    const LoadingConstructor = Vue.extend(Loading);
    let instance = new LoadingConstructor({
      el: document.createElement('div'),
    })
    document.body.appendChild(instance.$el)
    Vue.prototype.$loading = instance;

    // 完善一点应该是return一个 {show(){
    //    创建实例，Loading里面的close函数销毁实例
    //  }
    // }
  }
}

export default LoadingPlugin;
