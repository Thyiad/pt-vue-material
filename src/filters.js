import {formatDate as date} from './utils/tools'

const filters = {
    date,
}

const install = Vue=>{
    Object.keys(filters).forEach(key=>{
        Vue.filter(key, filters[key])
    })
}

export default {install}
