import DashboardLayout from '@/pages/Dashboard/Layout/DashboardLayout.vue'
import AuthLayout from '@/pages/Dashboard/Pages/AuthLayout.vue'

import NotFound from '@/pages/Dashboard/NotFound.vue';
import Forget from '@/pages/Dashboard/Pages/Forget.vue';
import Dashboard from '@/pages/Dashboard/Dashboard.vue';

const Login = ()=>  import('@/pages/Dashboard/Pages/Login.vue');
const Register = ()=>  import('@/pages/Dashboard/Pages/Register.vue');
const Profile = ()=>  import('@/pages/Dashboard/Pages/Profile.vue');
const TableList = ()=>  import('@/pages/Dashboard/Pages/TableList.vue');

let authPages = {
  path: '/',
  component: AuthLayout,
  name: 'Authentication',
  children: [
    {
      path: '/login',
      name: 'Login',
      components: {default: Login},
      meta: {
        title: '登录'
      }
    },
    {
      path: '/register',
      name: 'Register',
      components: {default: Register},
      meta: {
        title: '注册'
      }
    },
    {
      path: '/forget',
      name: 'Forget',
      components: {default: Forget},
      meta: {
        title: '忘记密码',
      }
    },
  ]
}

const routes = [
  {
    path: '/',
    redirect: '/dashboard',
    name: 'Home'
  },
  authPages,
  {
    path: '/',
    component: DashboardLayout,
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        components: {default: Dashboard},
        meta:{
          title: '首页'
        }
      },
      {
        path: 'profile',
        name: 'Profile',
        components: {default: Profile},
        meta:{
          title: '个人中心'
        }
      },
      {
        path: 'tableList',
        name: 'TableList',
        components: {default: TableList},
        meta: {
          title: '表格列表'
        }
      },
    ]
  },
  {
    path: '*',
    component: NotFound,
  }
];

export default routes
