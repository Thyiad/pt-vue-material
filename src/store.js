import Vue from 'vue';
import Vuex from 'vuex';
import * as constant from '@/constants/index'
import * as mutationKeys from '@/constants/mutation-keys';
import * as actionTypes from '@/constants/action-types';

import {toast, confirm, alert} from "@/utils/tools";
import {post} from '@/utils/ajax';
import * as api from '@/constants/api';

Vue.use(Vuex);

const state = {
    userInfo: {},
}

const getters = {
    isAdmin(state){
        return state.userInfo.role==='admin';
    }
}

const mutations = {
    [mutationKeys.SET_USER_INFO](state, userInfo){
        state.userInfo = userInfo;
    },
}

const actions = {
    [actionTypes.INIT_USER_INFO]({commit}){
        post(api.GET_USER_INFO).then(data=>{
            commit(mutationKeys.SET_USER_INFO, data);
        })
    }
}

const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions,
})

export default store
