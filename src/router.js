import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes/routes';
import auth from './utils/auth'

Vue.use(Router)

const router = new Router({
  routes, // short for routes: routes
  linkExactActiveClass: 'nav-item active'
});

auth.init(router);

export default router;
