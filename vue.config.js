const publicPathDic = {
    dev: '/',
    test: '//staticdaily.zhongan.com/website/boilerplate/test',
    pre: '//staticdaily.zhongan.com/website/boilerplate/pre',
    prd: '//staticdaily.zhongan.com/website/boilerplate/prd',
}

const appEnv = process.env.VUE_APP_ENV;
console.log('VUE_APP_ENV: '+appEnv);

module.exports = {
    chainWebpack: config => {
        config.module.rules.delete('eslint');
    },
    devServer:{
        disableHostCheck: true,
    },
    publicPath: publicPathDic[appEnv]||'/',
}