### vue-material
vue router必须要低于3.0.2，因为会跟vue-material 1.0.2冲突：
https://github.com/vuematerial/vue-material/issues/1977

最新v1.3.2已经解决该问题

### 预览

![](./preview-login.jpg)
![](./preview.jpg)